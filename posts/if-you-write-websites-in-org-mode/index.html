<head>
    <title>If you write websites in Org Mode...</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/info.css">
    <link rel="stylesheet" href="/css/navbar.css">
    <link rel="stylesheet" href="/css/post-list.css">
    <link rel="stylesheet" href="/css/links.css">
    
    <meta charset="UTF-8"/>
</head>

<nav class="navbar">
    
    <a href="/">Home</a>
    <a href="/posts">Posts</a>
    <a href="/tools">Tools</a>
    <a href="/tags">Tags</a>
    <a href="/contact">Contact</a>
    <a href="https://notes.sandvich.xyz">notes.sandvich.xyz</a>
</nav>

<div class="root">
    <div class="content">
        <h1 class="MUGA">
            If you write websites in Org Mode...
        </h1>
        
            <table class="info">
    <tbody>
        <tr class="info-date">
            <td><i class="bi bi-clock"></i></td>
            <td>2022-09-10</td>
        </tr>
        <tr class="info-reading-time">
            <td><i class="bi bi-hourglass"></i></td>
            <td>Reading time 4 min.</td>
        </tr>
        <tr class="info-tags">
            <td><i class="bi bi-tags"></i></td>
            <td>
                
                    
                    <a class="tag" href="/tags/emacs">emacs</a>
                
                    
                    <a class="tag" href="/tags/tutorial">tutorial</a>
                
            </td>
        </tr>
    </tbody>
</table>

        
        
<p>
If you&#39;re an Emacs user, you would probably already know how powerful Org Mode is. You can organize notes, manage to-do lists, and write articles. Almost anything can be written with Org Mode. <a href="https://notes.sandvich.xyz">I write my lecture notes in Org Mode</a>. Even this entire article was written in Org Mode.</p>
<img src="../../assets/org-setup.png" alt="../assets/org-setup.png" title="../assets/org-setup.png"/>
<p>
However, you&#39;re probably still using the built-in Emacs HTML exporter to generate HTML files or even Pandoc to convert Org to HTML.</p>
<p>
An even better way to generate your content is with Hugo.</p>
<div id="outline-container-headline-1" class="outline-2">
<h2 id="headline-1">
Hugo
</h2>
<div id="outline-text-headline-1" class="outline-text-2">
<p>
Hugo is a fast and flexible static site generator written in Go. It does this by using Go templates, which allows Hugo to generate pages <a href="https://forestry.io/blog/hugo-vs-jekyll-benchmark/">significantly faster than Jekyll</a> and be extremely extensible at the same time. Whenever someone visits your site, they are not going to believe that it&#39;s all statically generated. It is really that powerful that I&#39;ve decided to abandon my own Pandoc-based static site generator and move to Hugo.</p>
<p>
The main selling point of Hugo for me is org-mode support. Hugo can automatically generate content from org-mode files. I know that Emacs can already do that, but Hugo automates and manages the entire site generation process. It&#39;s easier to have something that does the job rather than having to write some Elisp to manage it for you.</p>
<p>
Most other static site generators do not have org-mode support. If you want to use org-mode to write your website, you&#39;ll either have to stick with Emacs&#39;s HTML exporter, write your own with Pandoc, or use Hugo.</p>
</div>
</div>
<div id="outline-container-headline-2" class="outline-2">
<h2 id="headline-2">
Setting up hugo
</h2>
<div id="outline-text-headline-2" class="outline-text-2">
<p>
The hardest part about Hugo is setting it up to be usable and generate a website. However, many people in the Hugo community have built themes for you to choose, so you do not have to worry about writing your own.</p>
<p>
I continue to write my own themes, but a couple I would recommend are <a href="https://github.com/adityatelange/hugo-PaperMod">PaperMod</a> (for something that is attractive and works) and <a href="https://github.com/LukeSmithxyz/lugo">Lugo</a> (for simple websites that you quickly want running).</p>
<div id="outline-container-headline-3" class="outline-3">
<h3 id="headline-3">
If you want to write your own theme
</h3>
<div id="outline-text-headline-3" class="outline-text-3">
<p>
It is not difficult, but it is very time-consuming. If you&#39;re going to develop a theme, you should use the <code>web-mode</code> package because it supports Go&#39;s templating syntax.</p>
<div class="src src-emacs-lisp">
<div class="highlight"><div style="color:#f8f8f2;background-color:#282a36;-moz-tab-size:4;-o-tab-size:4;tab-size:4;">
<table style="border-spacing:0;padding:0;margin:0;border:0;"><tr><td style="vertical-align:top;padding:0;margin:0;border:0;">
<pre tabindex="0" style="color:#f8f8f2;background-color:#282a36;-moz-tab-size:4;-o-tab-size:4;tab-size:4;"><code><span style="white-space:pre;user-select:none;margin-right:0.4em;padding:0 0.4em 0 0.4em;color:#7f7f7f">1
</span></code></pre></td>
<td style="vertical-align:top;padding:0;margin:0;border:0;;width:100%">
<pre tabindex="0" style="color:#f8f8f2;background-color:#282a36;-moz-tab-size:4;-o-tab-size:4;tab-size:4;"><code class="language-emacs-lisp" data-lang="emacs-lisp"><span style="display:flex;"><span>(<span style="color:#8be9fd;font-style:italic">use-package</span> <span style="color:#8be9fd;font-style:italic">web-mode</span>)</span></span></code></pre></td></tr></table>
</div>
</div>
</div>
<p>
To get started, check <a href="https://gohugo.io/templates/introduction/">Hugo&#39;s documentation</a> and <a href="https://golang.org/pkg/text/template/">Go documentation</a> for more information about how the templating system works. Depending on how much time you have, the fundamentals aren&#39;t that useful. The later sections show more practical examples.</p>
</div>
</div>
</div>
</div>
<div id="outline-container-headline-4" class="outline-2">
<h2 id="headline-4">
Formatting your Org files
</h2>
<div id="outline-text-headline-4" class="outline-text-2">
<p>
Hugo documents most of their examples with Markdown and use YAML or TOML to document their front matter. However, you can use Org&#39;s export options as your front matter.</p>
<div class="src src-org">
<div class="highlight"><div style="color:#f8f8f2;background-color:#282a36;-moz-tab-size:4;-o-tab-size:4;tab-size:4;">
<table style="border-spacing:0;padding:0;margin:0;border:0;"><tr><td style="vertical-align:top;padding:0;margin:0;border:0;">
<pre tabindex="0" style="color:#f8f8f2;background-color:#282a36;-moz-tab-size:4;-o-tab-size:4;tab-size:4;"><code><span style="white-space:pre;user-select:none;margin-right:0.4em;padding:0 0.4em 0 0.4em;color:#7f7f7f">1
</span><span style="white-space:pre;user-select:none;margin-right:0.4em;padding:0 0.4em 0 0.4em;color:#7f7f7f">2
</span><span style="white-space:pre;user-select:none;margin-right:0.4em;padding:0 0.4em 0 0.4em;color:#7f7f7f">3
</span><span style="white-space:pre;user-select:none;margin-right:0.4em;padding:0 0.4em 0 0.4em;color:#7f7f7f">4
</span><span style="white-space:pre;user-select:none;margin-right:0.4em;padding:0 0.4em 0 0.4em;color:#7f7f7f">5
</span></code></pre></td>
<td style="vertical-align:top;padding:0;margin:0;border:0;;width:100%">
<pre tabindex="0" style="color:#f8f8f2;background-color:#282a36;-moz-tab-size:4;-o-tab-size:4;tab-size:4;"><code class="language-org" data-lang="org"><span style="display:flex;"><span><span style="color:#6272a4">#+TITLE</span><span style="color:#6272a4">: How to Become Successful</span>
</span></span><span style="display:flex;"><span><span style="color:#6272a4">#+AUTHOR</span><span style="color:#6272a4">: Velcuz</span>
</span></span><span style="display:flex;"><span>#+TAGS[]: muga funny finnish independent forsen
</span></span><span style="display:flex;"><span>
</span></span><span style="display:flex;"><span>Hello guys. I will discuss how you can become rich and successful.</span></span></code></pre></td></tr></table>
</div>
</div>
</div>
</div>
</div>
<div id="outline-container-headline-5" class="outline-2">
<h2 id="headline-5">
Images
</h2>
<div id="outline-text-headline-5" class="outline-text-2">
<p>
The way images work in Hugo are a bit finicky. You can put them in <code>static/</code> which is copied to the root directory of the built website, meaning you&#39;d have to reference them with <code>/path-from-static</code>.</p>
<p>
If you put them in the same directory as your org files, you can reference them with <code>./relative-path.png</code>, but they won&#39;t actually appear since your org files are now built in a directory deeper.</p>
<p>
For images to actually appear in both Emacs and your website, you should make a directory for the post, and inside should be your Org content in an <code>index.org</code> file.</p>
<p>
For example, you can create a directory structure like so:</p>
<pre class="example">
your-website/
├─ content/
│  ├─ my-cool-articles/
│  │  ├─ top-5-images/
│  │  │  ├─ index.org
│  │  │  ├─ funny-image.png
│  │  ├─ another-top-5/
│  │  │  ├─ index.org
│  │  │  ├─ another-image.png
│  │  ├─ top-5-quotes.org
</pre>
<div id="outline-container-headline-6" class="outline-3">
<h3 id="headline-6">
WORKAROUND UPDATED 11 SEPTEMBER
</h3>
<div id="outline-text-headline-6" class="outline-text-3">
<p>
Instead of having to put org files into their own directory, you can specify the URL with an <code>#+attr_html</code> option.</p>
<div class="src src-org">
<div class="highlight"><div style="color:#f8f8f2;background-color:#282a36;-moz-tab-size:4;-o-tab-size:4;tab-size:4;">
<table style="border-spacing:0;padding:0;margin:0;border:0;"><tr><td style="vertical-align:top;padding:0;margin:0;border:0;">
<pre tabindex="0" style="color:#f8f8f2;background-color:#282a36;-moz-tab-size:4;-o-tab-size:4;tab-size:4;"><code><span style="white-space:pre;user-select:none;margin-right:0.4em;padding:0 0.4em 0 0.4em;color:#7f7f7f">1
</span><span style="white-space:pre;user-select:none;margin-right:0.4em;padding:0 0.4em 0 0.4em;color:#7f7f7f">2
</span></code></pre></td>
<td style="vertical-align:top;padding:0;margin:0;border:0;;width:100%">
<pre tabindex="0" style="color:#f8f8f2;background-color:#282a36;-moz-tab-size:4;-o-tab-size:4;tab-size:4;"><code class="language-org" data-lang="org"><span style="display:flex;"><span><span style="color:#6272a4">#+attr_org</span><span style="color:#6272a4">: :src ../relative-path/image.png</span>
</span></span><span style="display:flex;"><span>[[<span style="color:#50fa7b">./relative-path/image.png</span>]]</span></span></code></pre></td></tr></table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

        <h2 id="comments">Comments</h2>
        <div id="remark42">
</div>
<script>
 var remark_config = {
     host: "https://remark.sandvich.xyz",
     site_id: "sandvich",
     url: "https://sandvich.xyz/posts/if-you-write-websites-in-org-mode/",
     locale: "en",
     
 };

 (function(c) {
     for(var i = 0; i < c.length; i++){
         var d = document, s = d.createElement('script');
         s.src = remark_config.host + '/web/' + c[i] +'.js';
         s.defer = true;
         (d.head || d.body).appendChild(s);
     }
 })(remark_config.components || ['embed']);
</script>

        <div id="footer">
    <hr>
     <center>
         who is he talking to <img src="/LULE.png" style="vertical-align: middle;">
     </center>
</div>

    </div>
</div>
<ul class="shortlist links">
    <li>
    <a href="https://github.com/humanoidsandvichdispenser">
        <i class="bi bi-github"></i>
        <span class="more">
            HumanoidSandvichDispenser
        </span>
    </a>
</li>
<li>
    <a href="mailto:humanoidsandvichdispenser@gmail.com">
        <i class="bi bi-envelope-fill"></i>
        <span class="more">
            humanoidsandvichdispenser@gmail.com
        </span>
    </a>
</li>
<li>
    <a href="/sandvich.gpg">
        <i class="bi bi-key-fill"></i>
        <span class="more">
            Encrypt mail with GPG key
        </span>
    </a>
</li>
<li>
    <a href="/posts/index.xml">
        <i class="bi bi-rss-fill"></i>
        <span class="more">
            RSS Feed (posts only)
        </span>
    </a>
</li>

</ul>
